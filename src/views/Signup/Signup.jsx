import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import logo from '../../logo.svg';
import '../../assets/css/Signup.css';
import { Form, FormGroup, Input, Label, Button } from 'reactstrap';
import * as routes from '../../routes/routes';
import { fire, auth } from '../../firebase/firebase';


class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      contact: '',
      password: '',
      cfmpassword: '',
      error: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.pushUserToFirebase = this.pushUserToFirebase.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);   
    this.handleReset = this.handleReset.bind(this);
  }

  handleChange = text => {
    this.setState({ [text.target.name]: text.target.value });
  }

  pushUserToFirebase(){
    var user = fire.auth().currentUser;
    var timestamp = Math.floor(Date.now() / 1000);
    var actualDateTime = new Date(parseInt(timestamp, 0) * 1000).toString();

    user.updateProfile({
      displayName: this.state.name
    }).catch(function (error) {
      console.log(error.message)
    });

    var newUserData = {
      name: this.state.name,
      email: auth.currentUser.email,
      contact: this.state.contact,
      createdOn: actualDateTime,
      lastUpdate: "-",
      createdOnTimestamp: parseInt(timestamp, 0),
      lastUpdateTimestamp: "-",
    };

    fire.database()
      .ref("RegisteredAccountDetails")
      .child(auth.currentUser.uid)
      .set(newUserData);
  }

  handleSubmit(event) {
    const { name, email, contact, password, cfmpassword } = this.state;
    const { history } = this.props;
    var regex = /^[a-z0-9" "]+$/i;  // For name: only letters, numbers and spacing
    var regexName = /^[" "]+$/i;    // For name: contain only spacing
    var regexEmail = /^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/;  // For email: valid email include must contain/does not repeat "@" and "."

    if(regexName.test(name) === true){
      alert("Please enter something for Name!");
    }
    else{
      if(email.length > 40){
        alert("Sorry, Maximum of 40 characters only for Email!");
      }
      else{
        if(regexEmail.test(email) === false){
          alert("Please ensure your Email is valid!");
        }
        else{
          if(regex.test(name) === false){
            alert("Please enter alphanumeric only for Name!");
          }
          else{
            if(name.length > 30){
              alert("Sorry, Maximum of 30 characters only for Name!");
            }
            else{
              if(password !== cfmpassword){
                alert("Please ensure the Password and Confirm Password is the same!");
              }
              else{
                if(contact.length > 7 && contact.length < 21){
                  fire.database().ref("RegisteredAccountDetails").orderByKey().once("value")
                  .then(snapshot => {
                    snapshot.forEach(snap => {
                      if(snap.val().name !== name){
                        if(snap.val().email !== email){
                          if(snap.val().contact !== contact){
                            auth.createUserWithEmailAndPassword(email, password)
                            .then(() => {
                              this.pushUserToFirebase();
                    
                              alert("Account Created");
                    
                              history.push(routes.Dashboard);
                            }).catch(error => {
                              this.setState({error});
                    
                              setTimeout(() => {
                                this.setState({error: ''});
                              }, 3000);
                            })
                          }
                          else{
                            alert("Sorry, The Contact.No is already taken!");
                          }
                        }
                        else{
                          alert("Sorry, The Email is already taken!");
                        }
                      }
                      else{
                        alert("Sorry, The name is already taken!");
                      }
                    })
                  })
                }
                else{
                  alert("Please enter 8-20 digits for Contact.No!");
                }
              }
            }
          }
        }
      }
    }
    
    event.preventDefault();
  }

  handleReset() {
    this.setState({
      name: '',
      email: '',
      contact: '',
      password: '',
      cfmpassword: ''
    });
  }

  render(){
    const { error } = this.state;

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />

          <h1 id="title">Signup</h1><br />
          
          <Form onSubmit={this.handleSubmit}>
            <FormGroup className="rowText">
              <Label for="name">Name:</Label>

              <Input
                required={true}
                type="text"
                name="name"
                onChange={this.handleChange} />
            </FormGroup>

            <FormGroup className="rowText">
              <Label for="email">Email:</Label>

              <Input
                required={true}
                type="email"
                name="email"
                onChange={this.handleChange} />
            </FormGroup>

            <FormGroup className="rowText">
              <Label for="contact">Contact No:</Label>

              <Input
                required={true}
                type="number"
                name="contact"
                onChange={this.handleChange} />
            </FormGroup>

            <FormGroup className="rowText">
              <Label for="password">Password:</Label>

              <Input
                required={true}
                type="password"
                name="password"
                onChange={this.handleChange} />
            </FormGroup>

            <FormGroup className="rowText">
              <Label for="cfmpassword">Confirm Password:</Label>

              <Input
                required={true}
                type="password"
                name="cfmpassword"
                onChange={this.handleChange} />
            </FormGroup><br />

            <Button id="submit">Submit</Button>            
            <Button id="reset" onClick={this.handleReset} type="reset">Reset</Button>
          </Form><br />
          
          {error && <span id="error">{error.message}</span>}<br /><br />
        </header>
      </div>
    );
  }
}

export default withRouter(Signup);