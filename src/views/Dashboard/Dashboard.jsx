import React, { Component } from "react";
import logo from '../../logo.svg';
import { fire, auth } from '../../firebase/firebase';
import '../../assets/css/Dashboard.css';
import withAuthorization from "../../components/FirebaseAuthorization/withAuthorization";


class Dashboard extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      numberofGame: 0,
      noData: true
    };
  }

  componentWillMount() {
    this._isMounted = true;

    fire.database().ref("GameList").child(auth.currentUser.uid).orderByChild("Name").once("value")
    .then(snapshot => {
      if(this._isMounted){
        if(snapshot.exists()){
          var tempArray = [];
  
          snapshot.forEach(snap => {
            tempArray.push(snap.val());
          })
  
          this.setState({
              numberofGame: tempArray.length,
              noData: false
          });
        }
        else{
          this.setState({
            numberofGame: 0,
            noData: false
          });
        }
      }
    })
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    return (
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 id="title">Dashboard</h1><br />

            <div id="details">
              <div className="contentDetails" id="first"><p>1) Add Your Game</p></div>
              <div className="contentDetails">=></div>
              <div className="contentDetails" id="second"><p>2) Choose Your Game</p></div>
              <div className="contentDetails">=></div>
              <div className="contentDetails" id="third"><p>3) Edit The Game Choices</p></div>
            </div><br />

            <span>{this.state.noData ?
              <div className="message">
                <h5>Number of Game Added: </h5>
                <p style={{fontSize: "25px"}}>Loading...</p>
              </div>
              :
              <div className="message">
                <h5>Number of Game Added: </h5>
                <p id="numofGame">{this.state.numberofGame}</p>
              </div>
            }</span><br /><br />
          </header>
        </div>
    );
  }
}

const authCondition = authUser => !!authUser;
export default withAuthorization(authCondition)(Dashboard);