import React, { Component } from 'react';
import logo from '../../logo.svg';
import '../../routes/routes';
import '../../assets/css/Errors.css';


class PageNotFound extends Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1 id="title">Page Not Found</h1>

                    <h5 className="rowText">
                        The Page you are looking or doesn't exist or an other error occured.
                    </h5><br />

                    <h6 className="rowText">
                        <a href='/dashboard'>Return to HOME PAGE</a>
                    </h6><br /><br />
                </header>
            </div>
        );
    }
}

export default PageNotFound;