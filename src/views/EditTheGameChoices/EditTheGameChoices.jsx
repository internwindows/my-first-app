import React, { Component } from 'react';
// Authorization
import withAuthorization from "../../components/FirebaseAuthorization/withAuthorization";
// Firebase
import { fire, auth } from "../../firebase/firebase";
import '../../assets/css/EditTheGameChoices.css';
import logo from '../../logo.svg';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import _ from "lodash";
import { FormControl, Button } from 'react-bootstrap';
import Multiselect from 'react-widgets/lib/Multiselect';


class MyGameModalEditor extends React.Component {     // When edit column "MyGame", it return Modal with Multiselect under (Sub)Class "MyGameModalEditor",
  _isMounted = false;

  constructor(props) {                                // Any edit will then return back to onAfterSaveCell in (Main)Class "EditTheGameChoices"
    super(props);
    this.state = {
      name: [],
      defaultName: props.defaultValue,      // This bring back the value from the column "MyGame" from onAfterSaveCell in (Main)Class "EditTheGameChoices"
      open: true,
      GameList: []
    };

    this.updateData = this.updateData.bind(this);
  }

  componentWillMount() {
    this._isMounted = true;

    fire.database().ref("GameList").child(auth.currentUser.uid).orderByChild("Name").once("value")
    .then(snapshot => {
      if(this._isMounted){
        var tempArray = [];

        snapshot.forEach(snap => {
          tempArray.push(snap.val());
        });

        this.setState({
          GameList: tempArray
        });
      }
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  focus() {
    this.refs.inputRef.focus();
  }

  updateData() {
    if(this.state.name[0] !== undefined){
      var MyGame = [];

      this.state.name.forEach(snap => {
        MyGame.push(snap.name);
      })

      setTimeout(() => {
        this.props.onUpdate(MyGame.toString());
      }, 500);
    }
    else{
      alert("Please select your game");
    }
  }

  close = () => {
    this.setState({ open: false });
    this.props.onUpdate(this.state.defaultName);
  }
  
  render() {
    const fadeIn = this.state.open ? 'in' : '';
    const display = this.state.open ? 'block' : 'none';

    const selectedGame = _.map(this.state.GameList, data => {
      return (
        {
          id: data,
          name: data
        }
      )
    });

    return (
      <div className={ `modal fade ${fadeIn}` } id='myModal' role='dialog' style={ { display } }>
        <div className='modal-dialog'>
          <div className='modal-content'>

            <div className='modal-body'>
              <div ref='inputRef'
                className={ ( this.props.editorClass || '') + ' form-control editor edit-text' }
                style={ { display: 'inline', width: '50%' } }>Edit Your Game</div>

              <Multiselect
                data={selectedGame}
                valueField='id'
                textField='name'
                onChange={name => this.setState({ name })} />
            </div>
            
            <div className='modal-footer'>
              <button type='button' className='btn btn-primary' onClick={ this.updateData }>Save</button>
              <button type='button' className='btn btn-default' onClick={ this.close }>Close</button>
            </div>
            
          </div>
        </div>
      </div>
    );
  }
}

class EditTheGameChoices extends Component{                   // This is the Main Class that show on the page
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      Dataloading: true,
      noDataFound: true,

      Choiceloading: true,
      noChoiceFound: true,

      showGameList: [],
      selectedGameList: '',
      showGameData: [],

      showChoicesLength: 0,
      TotalChoicesLength: 0,
      showGameChoices: []
    };

    this.detectOnChangeEvent = this.detectOnChangeEvent.bind(this);
    this.onAfterSaveCell = this.onAfterSaveCell.bind(this);
    this.onAfterDeleteRow = this.onAfterDeleteRow.bind(this);
  }

  componentWillMount() {
    this._isMounted = true;

    fire.database().ref("GameList").child(auth.currentUser.uid).orderByKey().once("value")
    .then(snapshot => {
      if(this._isMounted){
        if(snapshot.exists()){
          this.setState({
            showGameList: snapshot.val(),
            Dataloading: false,
            noDataFound: false
          })
        }
        else{
          if(this.state.noDataFound === true){
            this.setState({
              Dataloading: false
            });
          }
          else{
            this.setState({
              Dataloading: true
            });
          }
        }
      }
    })

    fire.database().ref("PlayerChoices").child(auth.currentUser.uid).orderByKey().once("value")
    .then(snapshot => {
      if(this._isMounted){
        var Choiceslength = [];

        snapshot.forEach(snap => {
          Choiceslength.push(snap.val());
        })

        if(snapshot.exists()){
          this.setState({
            showGameChoices: snapshot.val(),
            showChoicesLength: Choiceslength.length,
            TotalChoicesLength: Choiceslength.length,
            Choiceloading: false,
            noChoiceFound: false
          })
        }
        else{
          if(this.state.noChoiceFound === true){
            this.setState({
              Choiceloading: false
            });
          }
          else{
            this.setState({
              Choiceloading: true
            });
          }
        }
      }
    })
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  gameList(){
    return _.map(this.state.showGameList, data => {
      return <option key={data}>{data}</option>;
    });
  }

  loadingData() {
    if (this.state.Dataloading) {
      return (
        <div>
          <h2>Loading...</h2>
        </div>
      )
    } 
    else{
      return (
        <div>
          <h2>No Data Found</h2>
        </div>
      )
    }
  }

  detectOnChangeEvent(event){
    var GameChoiceArray = [];

    if(event.target.value === "null"){
      this.setState({
        showGameData: [],
        selectedGameList: '',
        showGameChoices: [],
        showChoicesLength: 0
      })
    }
    else{
      this.setState({
        selectedGameList: event.target.value
      })

      fire.database().ref("GameList").child(auth.currentUser.uid).orderByKey().once("value")
      .then(snapshot => {
        snapshot.forEach(snap => {
          if(snap.val() === this.state.selectedGameList){
            fire.database().ref("GameData").child(auth.currentUser.uid).equalTo(snap.key).orderByKey().once("value")
            .then(childsnap => {
                this.setState({
                  showGameData: childsnap.val()
                });
            })

            fire.database().ref("PlayerChoices").child(auth.currentUser.uid).orderByKey().once("value")
            .then(shot => {
              shot.forEach(childsnap => {
                if(childsnap.val().MyGame.includes(this.state.selectedGameList)){
                  GameChoiceArray.push(childsnap.val());             
                }
                else{
                  this.setState({
                    showGameChoices: []
                  })
                }
              })

              this.setState({
                showChoicesLength: GameChoiceArray.length,
                showGameChoices: GameChoiceArray
              })
            })
          }
        })
      })
    }
  }

  loadingChoice() {
    if (this.state.Choiceloading) {
      return (
        <div>
          <h2>Loading...</h2>
        </div>
      )
    } 
    else{
      return (
        <div>
          <h2>No Data Found</h2>
        </div>
      )
    }
  }

  onAfterSaveCell(row, cellName, cellValue) {
    var duplicate = [];
    var FoundData = "";
    var regex = /^[a-z0-9" "]+$/i;  // For name: only letters, numbers and spacing
    var regexName = /^[" "]+$/i;    // For name: contain only spacing
    var timestamp = Math.floor(Date.now() / 1000);
    var actualDateTime = new Date(parseInt(timestamp, 0) * 1000).toString();

    if(cellName === "CharacterName"){
      if(cellValue !== "" && regexName.test(cellValue) === false){
        fire.database().ref("PlayerChoices").child(auth.currentUser.uid).orderByKey().once("value")
        .then(snapshot => {
          snapshot.forEach(snap => {
            if(snap.val().CharacterName !== cellValue){
              duplicate.push("No");
            }
            else{
              duplicate.push("Yes");
            }

            if(snap.val().CharacterName === row["CharacterNameId"]){
              FoundData = snap.val().CharacterName;
            }
          })

          var duplicateName = duplicate.filter((value) => value === "Yes");             

          if(duplicateName[0] === "Yes" && FoundData === row["CharacterNameId"]){
            this.setState({})   // Need this to trigger the code 'row["CharacterName"] = row["CharacterNameId"]'
            row["CharacterName"] = row["CharacterNameId"]
            alert("Please ensure your Character Name is not duplicated!");
          }
          else{
            if(regex.test(cellValue) === true){
              if(cellValue.length < 31){
                fire.database().ref("PlayerChoices").child(auth.currentUser.uid).orderByKey().once("value")
                .then(snapshot => {
                  snapshot.forEach(snap => {
                    if(snap.val().CharacterName === row["CharacterNameId"] && snap.val().id === row["ChoiceId"]){
                      var updateCharacterName = {
                        CharacterName: cellValue,
                        createdOn: actualDateTime,
                        unixTimestamp: parseInt(timestamp, 0)
                      }

                      fire.database().ref("PlayerChoices").child(auth.currentUser.uid).child(snap.key).update(updateCharacterName)
                      .then(() => {
                        window.location.reload();
                      });
                    }
                  })
                })
              }
              else
              {
                this.setState({})   // Need this to trigger the code 'row["CharacterName"] = row["CharacterNameId"]'
                row["CharacterName"] = row["CharacterNameId"]
                alert("Sorry, Maximum of 30 characters only!");
              }
            }
            else
            {
              this.setState({})   // Need this to trigger the code 'row["CharacterName"] = row["CharacterNameId"]'
              row["CharacterName"] = row["CharacterNameId"]
              alert("Please enter alphanumeric only!");
            }
          }
        })
      }
      else
      {
        row["CharacterName"] = row["CharacterNameId"]
        alert("Please enter something to edit!");
      }
    }

    else if(cellName === "MyGame"){
      if(cellValue !== row["MyGameId"]){
        fire.database().ref("PlayerChoices").child(auth.currentUser.uid).orderByKey().once("value")
        .then(snapshot => {
          snapshot.forEach(snap => {
            if(snap.val().MyGame === row["MyGameId"] && snap.val().id === row["ChoiceId"]){
              var updateMyGame = {
                MyGame: cellValue,
                createdOn: actualDateTime,
                unixTimestamp: parseInt(timestamp, 0)
              }

              fire.database().ref("PlayerChoices").child(auth.currentUser.uid).child(snap.key).update(updateMyGame)
              .then(() => {
                window.location.reload();
              });
            }
          })
        })
      }
      else{
        alert("Please ensure your Game Sequence is not duplicated!");
      }
    }

    else if(cellName === "PreferCharacterGender"){
      if(cellValue === row["PreferCharacterGenderId"]){
        alert("Please ensure your Character Gender is not duplicated!");
      }
      else{
        fire.database().ref("PlayerChoices").child(auth.currentUser.uid).orderByKey().once("value")
        .then(snapshot => {
          snapshot.forEach(snap => {
            if(snap.val().PreferCharacterGender === row["PreferCharacterGenderId"] && snap.val().id === row["ChoiceId"]){
              var updateCharacterGender = {
                PreferCharacterGender: cellValue,
                createdOn: actualDateTime,
                unixTimestamp: parseInt(timestamp, 0)
              }

              fire.database().ref("PlayerChoices").child(auth.currentUser.uid).child(snap.key).update(updateCharacterGender)
              .then(() => {
                window.location.reload();
              });
            }
          })
        })
      }
    }

    else if(cellName === "PreferGamePlay"){
      if(cellValue === row["PreferGamePlayId"]){
        alert("Please ensure your Game Play is not duplicated!");
      }
      else{
        fire.database().ref("PlayerChoices").child(auth.currentUser.uid).orderByKey().once("value")
        .then(snapshot => {
          snapshot.forEach(snap => {
            if(snap.val().PreferGamePlay === row["PreferGamePlayId"] && snap.val().id === row["ChoiceId"]){
              var updateGamePlay = {
                PreferGamePlay: cellValue,
                createdOn: actualDateTime,
                unixTimestamp: parseInt(timestamp, 0)
              }

              fire.database().ref("PlayerChoices").child(auth.currentUser.uid).child(snap.key).update(updateGamePlay)
              .then(() => {
                window.location.reload();
              });
            }
          })
        })
      }
    }

    else if(cellName === "PreferinDesign"){
      if(cellValue === row["PreferinDesignId"]){
        alert("Please ensure your Design is not duplicated!");
      }
      else{
        fire.database().ref("PlayerChoices").child(auth.currentUser.uid).orderByKey().once("value")
        .then(snapshot => {
          snapshot.forEach(snap => {
            if(snap.val().PreferinDesign === row["PreferinDesignId"] && snap.val().id === row["ChoiceId"]){
              var updateDesign = {
                PreferinDesign: cellValue,
                createdOn: actualDateTime,
                unixTimestamp: parseInt(timestamp, 0)
              }

              fire.database().ref("PlayerChoices").child(auth.currentUser.uid).child(snap.key).update(updateDesign)
              .then(() => {
                window.location.reload();
              });
            }
          })
        })
      }
    }
  }

  onAfterDeleteRow(rowKeys){
    fire.database().ref("PlayerChoices").child(auth.currentUser.uid).orderByKey().once("value")
    .then(snapshot => {
      snapshot.forEach(snap => {
        rowKeys.forEach(childsnap => {
          if(snap.val().CharacterName === childsnap){
            fire.database().ref("PlayerChoices").child(auth.currentUser.uid).child(snap.key).remove()
            .then(() => {
              window.location.reload();
            })
          }
        })
      })
    })
  }

  render() {
    const gameData = _.map(this.state.showGameData, i => {
      return (
        {
          gameNameId: i.name,
          gameDateId: i.releaseDate,
          gamePaymentId: i.payment,
          gamePlatformId: i.platform,
          gameRatingId: i.rating,
          gameName: i.name,
          gameDate: i.releaseDate,
          gamePayment: i.payment,
          gamePlatform: i.platform,
          gameRating: i.rating
        }
      )
    });

    const Dataoptions = {
      noDataText: this.loadingData()
    };

    const gameChoice = _.map(this.state.showGameChoices, i => {
      return (
        {
          CharacterNameId: i.CharacterName,
          MyGameId: i.MyGame,
          PreferCharacterGenderId: i.PreferCharacterGender,
          PreferGamePlayId: i.PreferGamePlay,
          PreferinDesignId: i.PreferinDesign,
          CharacterName: i.CharacterName,
          MyGame: i.MyGame,
          PreferCharacterGender: i.PreferCharacterGender,
          PreferGamePlay: i.PreferGamePlay,
          PreferinDesign: i.PreferinDesign
        }
      )
    });

    const customTotal = (from, to, size) => (
      <span className="react-bootstrap-table-pagination-total" style={{float: "right"}}>
        Showing {from} to {to} of {size} Results
      </span>
    );

    const Choiceoptions = {
      afterDeleteRow: this.onAfterDeleteRow,
      page: 1,
      sizePerPageList: [{
        text: '5', value: 5
      }, {
        text: '10', value: 10
      }, {
        text: 'All', value: gameChoice.length
      }],
      sizePerPage: 5,
      pageStartIndex: 1,
      paginationSize: 5,
      prePage: '<',
      nextPage: '>',
      firstPage: '<<',
      lastPage: '>>',
      paginationShowsTotal: customTotal,
      noDataText: this.loadingChoice()
    };

    const selectRowProp = {
      mode: 'checkbox'
    };

    const cellEditProp = {
      mode: 'dbclick',
      blurToSave: true,
      afterSaveCell: this.onAfterSaveCell
    };

    const MyGameModal = (onUpdate, props) => (<MyGameModalEditor onUpdate={ onUpdate } {...props}/>);
    const Gender = [ 'Male', 'Female' ];
    const Play = [ 'Solo', 'MultiPlayer', 'Solo and MultiPlayer' ];
    const Design = [ '2D', '3D' ];

    return(
      <div className="App">
        <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 id="title">Edit The Game Choices</h1><br />

            <FormControl
              id="dropdown"
              className="content"
              componentClass="select"
              placeholder="select"
              onChange={this.detectOnChangeEvent}>

              <option value="null">
                Please Select Your Game
              </option>
              {this.gameList()}
            </FormControl>

            <Button id="reset" onClick={() => {window.location.reload(false)}} type="reset">Reset</Button>

            <div>
              <h4>This will be your Game Details</h4>

              <BootstrapTable bordered={false} data={gameData} options={Dataoptions} className="gameDetails">
                <TableHeaderColumn dataField='gameNameId' hidden isKey={true}></TableHeaderColumn>
                <TableHeaderColumn dataField='gameDateId' hidden></TableHeaderColumn>
                <TableHeaderColumn dataField='gamePaymentId' hidden></TableHeaderColumn>
                <TableHeaderColumn dataField='gamePlatformId' hidden></TableHeaderColumn>
                <TableHeaderColumn dataField='gameRatingId' hidden></TableHeaderColumn>
                <TableHeaderColumn dataField='gameName'>Game Name</TableHeaderColumn>
                <TableHeaderColumn dataField='gameDate'>Game Release Date</TableHeaderColumn>
                <TableHeaderColumn dataField='gamePayment'>Game Payment($)</TableHeaderColumn>
                <TableHeaderColumn dataField='gamePlatform'>Game Platform</TableHeaderColumn>
                <TableHeaderColumn dataField='gameRating'>Game Rating</TableHeaderColumn>
              </BootstrapTable><br />

              <h4>This will be your Game You Choose(Number of Choices: {this.state.showChoicesLength}, Total of Choices: {this.state.TotalChoicesLength})</h4>
              <h6>You can edit all the column by double-click the data!</h6><br />

              <BootstrapTable pagination={true} search={true} bordered={false} data={gameChoice} selectRow={selectRowProp} deleteRow options={Choiceoptions} cellEdit={cellEditProp} className="gameDetails">
                <TableHeaderColumn dataField='CharacterNameId' hidden isKey={true}></TableHeaderColumn>
                <TableHeaderColumn dataField='MyGameId' hidden></TableHeaderColumn>
                <TableHeaderColumn dataField='PreferCharacterGenderId' hidden></TableHeaderColumn>
                <TableHeaderColumn dataField='PreferGamePlayId' hidden></TableHeaderColumn>
                <TableHeaderColumn dataField='PreferinDesignId' hidden></TableHeaderColumn>
                <TableHeaderColumn dataSort dataField='CharacterName' tdStyle={{ whiteSpace: 'normal' }}>Character Name</TableHeaderColumn>
                <TableHeaderColumn dataSort dataField='MyGame' customEditor={{ getElement: MyGameModal }} tdStyle={{ whiteSpace: 'normal' }}>My Game</TableHeaderColumn>
                <TableHeaderColumn dataSort dataField='PreferCharacterGender' editable={{type:'select', options:{values: Gender}}}>Character Gender</TableHeaderColumn>
                <TableHeaderColumn dataSort dataField='PreferGamePlay' editable={{type:'select', options:{values: Play}}}>Game Play</TableHeaderColumn>
                <TableHeaderColumn dataSort dataField='PreferinDesign' editable={{type:'select', options:{values: Design}}}>Design</TableHeaderColumn>
              </BootstrapTable><br /><br />
            </div>
        </header>
      </div>
    )
  }
}

const authCondition = authUser => !!authUser;
export default withAuthorization(authCondition)(EditTheGameChoices);