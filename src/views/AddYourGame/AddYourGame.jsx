import React, { Component } from 'react';
// Authorization
import withAuthorization from "../../components/FirebaseAuthorization/withAuthorization";
// Firebase
import { fire, auth } from "../../firebase/firebase";
import '../../assets/css/AddYourGame.css';
import logo from '../../logo.svg';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import _ from "lodash";
import AddModal from "./AddModal";
import axios from "axios";


class AddYourGame extends Component{
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      noDataFound: true,
      showallGameData: []
    };

    this.onAfterSaveCell = this.onAfterSaveCell.bind(this);
  }

  componentWillMount() {
    this._isMounted = true;

    axios.get(process.env.REACT_APP_FIREBASE_DATABASE + "/GameData/" + auth.currentUser.uid +
    ".json?auth=" + process.env.REACT_APP_FIREBASE_DATABASE_SECRET)
    .then(response => {
      if(this._isMounted){
        if(response.data !== null){
          this.setState({
            showallGameData: response.data,
            loading: false,
            noDataFound: false
          });
        }
        else{
          if(this.state.noDataFound === true){
            this.setState({
              loading: false
            });
          }
          else{
            this.setState({
              loading: true
            });
          }
        }
      }
    });

    // var randomKey1 = fire.database().ref().push().key;    //Hardcoded Data Generate Once Only, because user can type anything on the textbox to save on the database
    // var randomKey2 = fire.database().ref().push().key;    //As then it won't be so logical for the GamePlatform if the user is trolling
    // var randomKey3 = fire.database().ref().push().key;    //.push() => It help you create a unique id along with the data you construct
    // var randomKey4 = fire.database().ref().push().key;
    // var randomKey5 = fire.database().ref().push().key;
    // var randomKey6 = fire.database().ref().push().key;
    // var randomKey7 = fire.database().ref().push().key;
    // var randomKey8 = fire.database().ref().push().key;

    // var platform1 = {                
    //   Platform: "Xbox One",
    //   id: randomKey1
    // }                                

    // var platform2 = {
    //   Platform: "PlayStation 4",
    //   id: randomKey2
    // }

    // var platform3 = {
    //   Platform: "Microsoft Windows",
    //   id: randomKey3,
    // }

    // var platform4 = {
    //   Platform: "Mac",
    //   id: randomKey4
    // }

    // var platform5 = {
    //   Platform: "IOS",
    //   id: randomKey5
    // }

    // var platform6 = {
    //   Platform: "Android",
    //   id: randomKey6
    // }

    // var platform7 = {
    //   Platform: "Wii U",
    //   id: randomKey7
    // }

    // var platform8 = {
    //   Platform: "Nintendo Switch",
    //   id: randomKey8
    // }

    // fire.database().ref("GamePlatform").child(randomKey1).set(platform1);
    // fire.database().ref("GamePlatform").child(randomKey2).set(platform2);
    // fire.database().ref("GamePlatform").child(randomKey3).set(platform3);
    // fire.database().ref("GamePlatform").child(randomKey4).set(platform4);
    // fire.database().ref("GamePlatform").child(randomKey5).set(platform5);
    // fire.database().ref("GamePlatform").child(randomKey6).set(platform6);
    // fire.database().ref("GamePlatform").child(randomKey7).set(platform7);
    // fire.database().ref("GamePlatform").child(randomKey8).set(platform8);
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  onAfterDeleteRow(rowKeys){
    for (var i = 0; i < rowKeys.length; i++) {
      fire.database().ref("GameList").child(auth.currentUser.uid).orderByValue().equalTo(rowKeys[i].toString()).once("value")
      .then(snapshot => {
        snapshot.forEach(snap => {
          fire.database().ref("GameList").child(auth.currentUser.uid).child(snap.key).remove()
          .then(() => {
            fire.database().ref("GameData").child(auth.currentUser.uid).child(snap.key).remove()
            .then(() => {
              window.location.reload();
            })
          })
        })
      })
    }
  }

  loadingData() {
    if (this.state.loading) {
      return (
        <div>
          <h2>Loading...</h2>
        </div>
      )
    } 
    else{
      return (
        <div>
          <h2>No Data Found</h2>
        </div>
      )
    }
  }

  onAfterSaveCell(row, cellName, cellValue) {
    var duplicate = [];
    var FoundData = "";
    var regex = /^[a-z0-9" "]+$/i;  // For name: only letters, numbers and spacing
    var regexName = /^[" "]+$/i;    // For name: contain only spacing
    var timestamp = Math.floor(Date.now() / 1000);
    var actualDateTime = new Date(parseInt(timestamp, 0) * 1000).toString();

    if(cellName === "gameName"){
      if(cellValue !== "" && regexName.test(cellValue) === false){
        fire.database().ref("GameData").child(auth.currentUser.uid).orderByKey().once("value")
        .then(snapshot => {
          snapshot.forEach(snap => {
            if(snap.val().name !== cellValue){
              duplicate.push("No");
            }
            else{
              duplicate.push("Yes");
            }

            if(snap.val().name === row["gameNameId"]){
              FoundData = snap.val().name;
            }
          })

          var duplicateName = duplicate.filter((value) => value === "Yes");             

          if(duplicateName[0] === "Yes" && FoundData === row["gameNameId"]){
            this.setState({})   // Need this to trigger the code 'row["gameName"] = row["gameNameId"]'
            row["gameName"] = row["gameNameId"]
            alert("Please ensure your Game Name is not duplicated!");
          }
          else{
            if(regex.test(cellValue) === true){
              if(cellValue.length < 31){
                fire.database().ref("GameList").child(auth.currentUser.uid).orderByKey().once("value")
                .then(snap => {
                  snap.forEach(childsnap => {
                    if(childsnap.val() === row["gameNameId"]){
                      var updateGameList = {
                        [childsnap.key]: cellValue
                      }
  
                      fire.database().ref("GameList").child(auth.currentUser.uid).update(updateGameList)
                      .then(() => {
                        var updateGameDetails = {
                          createdDate: actualDateTime,
                          createdTimestamp: parseInt(timestamp, 0),
                          name: cellValue
                        }
        
                        fire.database().ref("GameData").child(auth.currentUser.uid).child(childsnap.key).update(updateGameDetails)
                        .then(() => {
                          window.location.reload();
                        });
                      })
                    }
                  })
                })
              }
              else
              {
                this.setState({})   // Need this to trigger the code 'row["gameName"] = row["gameNameId"]'
                row["gameName"] = row["gameNameId"]
                alert("Sorry, Maximum of 30 characters only!");
              }
            }
            else
            {
              this.setState({})   // Need this to trigger the code 'row["gameName"] = row["gameNameId"]'
              row["gameName"] = row["gameNameId"]
              alert("Please enter alphanumeric only!");
            }
          }
        })
      }
      else
      {
        row["gameName"] = row["gameNameId"]
        alert("Please enter something to edit!");
      }
    }

    else if(cellName === "gameDate"){
      var myDate = new Date(cellValue);
      var gameDateTimestamp = myDate.getTime()/1000;
      var gameDateactual = new Date(parseInt(gameDateTimestamp, 0) * 1000).toString();

      if(gameDateactual === "Invalid Date"){
        this.setState({})   // Need this to trigger the code 'row["gameDate"] = row["gameDateId"]'
        row["gameDate"] = row["gameDateId"];
        alert("Please ensure to input all valid fields including the time(AM:PM) also!");
      }
      else{
        row["gameDate"] = gameDateactual;

        fire.database().ref("GameList").child(auth.currentUser.uid).orderByKey().once("value")
        .then(snap => {
          snap.forEach(childsnap => {
            if(childsnap.val() === row["gameNameId"]){
              var updateGameDetails = {
                createdDate: actualDateTime,
                createdTimestamp: parseInt(timestamp, 0),
                releaseDate: gameDateactual,
                releaseDateTimestamp: parseInt(gameDateTimestamp, 0)
              }

              fire.database().ref("GameData").child(auth.currentUser.uid).child(childsnap.key).update(updateGameDetails)
              .then(() => {
                window.location.reload();
              });
            }
          })
        })
      }      
    }

    else if(cellName === "gamePayment"){
      var regexPayment = /^[1-9]{1,}[0-9]*$/i;    // For Game Payment: contain only numbers and also first digit start with 1-9

      if(parseInt(cellValue) === row["gamePaymentId"]){
        row["gamePayment"] = row["gamePaymentId"];
        alert("Please ensure your Game Payment is not duplicated!");
      }
      else{
        if(regexPayment.test(cellValue) === true){
          if(cellValue.length < 3){
            fire.database().ref("GameList").child(auth.currentUser.uid).orderByKey().once("value")
            .then(snap => {
              snap.forEach(childsnap => {
                if(childsnap.val() === row["gameNameId"]){
                  var updateGameDetails = {
                    createdDate: actualDateTime,
                    createdTimestamp: parseInt(timestamp, 0),
                    payment: parseInt(cellValue)
                  }
  
                  fire.database().ref("GameData").child(auth.currentUser.uid).child(childsnap.key).update(updateGameDetails)
                  .then(() => {
                    window.location.reload();
                  });
                }
              })
            })
          }
          else{
            this.setState({})   // Need this to trigger the code 'row["gamePayment"] = row["gamePaymentId"]'
            row["gamePayment"] = row["gamePaymentId"];
            alert("Sorry, Game Payment should be less than $100!");
          }
        }
        else{
          this.setState({})   // Need this to trigger the code 'row["gamePayment"] = row["gamePaymentId"]'
          row["gamePayment"] = row["gamePaymentId"];
          alert("Please ensure your Game Payment contains only numbers, \nAnd also must start with at least $1!");
        }
      }
    }

    else if(cellName === "gamePlatform"){
      if(cellValue === row["gamePlatformId"]){
        alert("Please ensure your Game Platform is not duplicated!");
      }
      else{
        fire.database().ref("GameList").child(auth.currentUser.uid).orderByKey().once("value")
        .then(snap => {
          snap.forEach(childsnap => {
            if(childsnap.val() === row["gameNameId"]){
              var updateGameDetails = {
                createdDate: actualDateTime,
                createdTimestamp: parseInt(timestamp, 0),
                platform: cellValue
              }

              fire.database().ref("GameData").child(auth.currentUser.uid).child(childsnap.key).update(updateGameDetails)
              .then(() => {
                window.location.reload();
              });
            }
          })
        })
      }
    }

    else if(cellName === "gameRating"){
      if(parseInt(cellValue) === row["gameRatingId"]){
        alert("Please ensure your Game Rating is not duplicated!");
      }
      else{
        fire.database().ref("GameList").child(auth.currentUser.uid).orderByKey().once("value")
        .then(snap => {
          snap.forEach(childsnap => {
            if(childsnap.val() === row["gameNameId"]){
              var updateGameDetails = {
                createdDate: actualDateTime,
                createdTimestamp: parseInt(timestamp, 0),
                rating: parseInt(cellValue)
              }

              fire.database().ref("GameData").child(auth.currentUser.uid).child(childsnap.key).update(updateGameDetails)
              .then(() => {
                window.location.reload();
              });
            }
          })
        })
      }
    }
  }

  render() {
    const gameData = _.map(this.state.showallGameData, i => {
      return (
        {
          gameNameId: i.name,
          gameDateId: i.releaseDate,
          gamePaymentId: i.payment,
          gamePlatformId: i.platform,
          gameRatingId: i.rating,
          gameName: i.name,
          gameDate: i.releaseDate,
          gamePayment: i.payment,
          gamePlatform: i.platform,
          gameRating: i.rating
        }
      )
    });

    const selectRowProp = {
      mode: 'checkbox'
    };

    const cellEditProp = {
      mode: 'dbclick',
      blurToSave: true,
      afterSaveCell: this.onAfterSaveCell
    };

    const customTotal = (from, to, size) => (
      <span className="react-bootstrap-table-pagination-total" style={{float: "right"}}>
        Showing {from} to {to} of {size} Results
      </span>
    );

    const options = {
      afterDeleteRow: this.onAfterDeleteRow,
      page: 1,
      sizePerPageList: [{
        text: '5', value: 5
      }, {
        text: '10', value: 10
      }, {
        text: 'All', value: gameData.length
      }],
      sizePerPage: 5,
      pageStartIndex: 1,
      paginationSize: 5,
      prePage: '<',
      nextPage: '>',
      firstPage: '<<',
      lastPage: '>>',
      paginationShowsTotal: customTotal,
      noDataText: this.loadingData()
    };

    const Platform = [ 'Xbox One', 'PlayStation 4', 'Microsoft Windows', 'Mac', 'IOS', 'Android', 'Wii U', 'Nintendo Switch' ];
    const Rating = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];

    return(
      <div className="App">
        <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 id="title">Add Your Game</h1>
            <h6>You can also edit all the column by double-click the data!</h6><br />

            <div>
              <BootstrapTable pagination={true} search={true} bordered={false} data={gameData} selectRow={selectRowProp} deleteRow options={options} cellEdit={cellEditProp} className="gameDetails">
                <TableHeaderColumn dataField='gameNameId' hidden isKey={true}></TableHeaderColumn>
                <TableHeaderColumn dataField='gameDateId' hidden></TableHeaderColumn>
                <TableHeaderColumn dataField='gamePaymentId' hidden></TableHeaderColumn>
                <TableHeaderColumn dataField='gamePlatformId' hidden></TableHeaderColumn>
                <TableHeaderColumn dataField='gameRatingId' hidden></TableHeaderColumn>
                <TableHeaderColumn dataSort dataField='gameName' tdStyle={{ whiteSpace: 'normal' }}>Game Name</TableHeaderColumn>
                <TableHeaderColumn dataSort dataField='gameDate' editable={{type:'datetime'}} tdStyle={{ whiteSpace: 'normal' }}>Game Release Date</TableHeaderColumn>
                <TableHeaderColumn dataSort dataField='gamePayment'>Game Payment($)</TableHeaderColumn>
                <TableHeaderColumn dataSort dataField='gamePlatform' editable={{type:'select', options:{values: Platform}}}>Game Platform</TableHeaderColumn>
                <TableHeaderColumn dataSort dataField='gameRating' editable={{type:'select', options:{values: Rating}}}>Game Rating(/10)</TableHeaderColumn>
              </BootstrapTable><br />
              
              <AddModal /><br /><br />
            </div>
        </header>
      </div>
    )
  }
}

const authCondition = authUser => !!authUser;
export default withAuthorization(authCondition)(AddYourGame);