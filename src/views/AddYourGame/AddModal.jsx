import React, { Component } from 'react';
import { Button, Modal, FormGroup, FormControl } from 'react-bootstrap';
import { fire, auth } from '../../firebase/firebase';
import '../../routes/routes';
import DatePicker from "react-datepicker";
import _ from "lodash";
import axios from "axios";
import Rating from "react-rating";


function FieldGroup({ id, label, help, ...props }) {
    return (
        <FormGroup controlId={id}>
            <div>{label}</div>
            <FormControl {...props} />
            {help && <div>{help}</div>}
        </FormGroup>
    );
}

class AddModal extends Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        this.state = {
            platformData: [],

            gameName: '',
            gamecalendarText: 'Select release Date & Time',
            gameselectedDate: "",
            gamedateTimestamp: 0,
            gamePayment: 0,
            gamePlatform: '',
            gameRating: 0,
            
            show: false,
            hidden: true
        };

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleDisplay = this.handleDisplay.bind(this);
        this.handleHide = this.handleHide.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.forceUpdateHandler = this.forceUpdateHandler.bind(this);
        this.gamechooseDateTime = this.gamechooseDateTime.bind(this);
        this.detectgameChangeEvent = this.detectgameChangeEvent.bind(this);
        this.gamePlatform = this.gamePlatform.bind(this);
        this.gameRating = this.gameRating.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;

        axios.get(process.env.REACT_APP_FIREBASE_DATABASE + "/GamePlatform/" + 
        ".json?auth=" + process.env.REACT_APP_FIREBASE_DATABASE_SECRET)
        .then(response => {
            if(this._isMounted){
                this.setState({
                    platformData: response.data
                })
            }
        });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    forceUpdateHandler() {
        this.forceUpdate();
    };

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }

    handleHide() {
        this.setState({ hidden: true })
        this.forceUpdateHandler()
    }

    handleDisplay() {
        this.setState({ hidden: false })
        this.forceUpdateHandler()
    }

    gamechooseDateTime(selected){
        var selectedDateTime = selected.getTime() / 1000;

        this.setState({
            gameselectedDate: selected,
            gamecalendarText: selected.toString(),
            gamedateTimestamp: parseInt(selectedDateTime, 0)
        })
    }

    detectgameChangeEvent(event){
        if(event.target.value === "null"){
            this.setState({
                gamePlatform: ''
            })
        }
        else{
            this.setState({
                gamePlatform: event.target.value
            })
        }
    }

    gamePlatform() {
        return _.map(this.state.platformData, data => {
            return <option key={data.id}>{data.Platform}</option>;
        });
    }

    gameRating(newRating){
        this.setState({ gameRating: newRating})
    }

    onSubmit(formEvent) {
        var duplicate = [];
        var regex = /^[a-z0-9" "]+$/i;  // For Game Name: only letters, numbers and spacing
        var regexName = /^[" "]+$/i;    // For Game Name: contain only spacing
        var regexPayment = /^[1-9]{1,}[0-9]*$/i;    // For Game Payment: contain only numbers and also first digit start with 1-9
        var timestamp = Math.floor(Date.now() / 1000);
        var actualDateTime = new Date(parseInt(timestamp, 0) * 1000).toString();
        var randomKey = fire.database().ref().push().key;

        if(regexName.test(this.state.gameName) === false){
            fire.database().ref("GameList").child(auth.currentUser.uid).orderByValue().once("value")
            .then(snapshot => {
                snapshot.forEach(snap => {
                    if(snap.val() !== this.state.gameName){
                        duplicate.push("No");
                    }
                    else{
                        duplicate.push("Yes");
                    }
                })

                var duplicateName = duplicate.filter((value) => value === "Yes");

                if(duplicateName[0] === "Yes"){
                    alert("Please ensure your Game Name is not duplicated!");
                }
                else{
                    if(regex.test(this.state.gameName) === true){
                        if(this.state.gameName.length < 31){
                            if(regexPayment.test(this.state.gamePayment) === true){
                                if(this.state.gamePayment.length < 3){
                                    fire.database().ref("GameList").child(auth.currentUser.uid).child(randomKey).set(this.state.gameName)
                                    .then(() => {
                                        var GameData = {
                                            name: this.state.gameName,
                                            releaseDate: this.state.gamecalendarText,
                                            releaseDateTimestamp: this.state.gamedateTimestamp,
                                            payment: parseInt(this.state.gamePayment),
                                            platform: this.state.gamePlatform,
                                            rating: this.state.gameRating,
                                            createdDate: actualDateTime,
                                            createdTimestamp: parseInt(timestamp, 0)
                                        }
    
                                        fire.database().ref("GameData").child(auth.currentUser.uid).child(randomKey).set(GameData)
                                        .then(() => {
                                            window.location.reload();
                                        });
                                    })
                                }
                                else{
                                    alert("Sorry, Game Payment should be less than $100!");
                                }
                            }
                            else{
                                alert("Please ensure your Game Payment start with at least $1!");
                            }
                        }
                        else{
                            alert("Sorry, Game Name allows maximum of 30 characters only!");
                        }
                    }
                    else
                    {
                        alert("Sorry, Game Name only allows alphanumeric!");
                    }
                }
            });
        }else{
            alert("Please enter something to edit!");
        }

        formEvent.preventDefault();
    }

    render() {
        const { gameName, gamecalendarText, gamePayment, gamePlatform, gameRating } = this.state;
        const isInvalid = gameName === '' || gamecalendarText === '' || gamePayment === 0  || gamePlatform === '' || gameRating === 0;

        return (
            <div>
                <Button onClick={this.handleShow} style={{ width: "80%", color: "white", borderColor: "white" }}>
                    <strong>Add Your Game</strong>
                </Button>

                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add Your Game</Modal.Title>
                        <small>Please be sure to fill up all the fields!</small>
                        <h4 style={{textDecoration: "underline"}}>Game Details</h4>                        
                    </Modal.Header>

                    <form onSubmit={this.onSubmit}>
                        <Modal.Body>
                            <FieldGroup
                                type="text"
                                label="Enter your Game Name:"
                                onChange={event => this.setState({ gameName: event.target.value })}
                            /><br />

                            <FormGroup style={{display: "flex"}}>
                                <p style={{fontSize: "14px", marginRight: "20px"}}>Set your Game Initial Release Date:</p>

                                <DatePicker
                                    placeholderText={this.state.gamecalendarText}
                                    selected={this.state.gameselectedDate}
                                    onChange={this.gamechooseDateTime}
                                    showTimeSelect
                                    dateFormat="Pp"/>
                            </FormGroup><br />

                            <FieldGroup
                                type="number"
                                label="Set your Game Payment($):"
                                onChange={event => this.setState({ gamePayment: event.target.value })}
                            /><br />

                            <FormGroup style={{display: "flex"}}>
                                <p style={{fontSize: "14px", marginRight: "20px"}}>Set your Game Platform:</p>

                                <FormControl
                                    componentClass="select"
                                    placeholder="select"
                                    onChange={this.detectgameChangeEvent}>

                                    <option value="null">
                                    </option>
                                    {this.gamePlatform()}
                                </FormControl>
                            </FormGroup><br />

                            <FormGroup style={{display: "flex"}}>
                                <p style={{fontSize: "14px", marginRight: "20px"}}>Set your Game Rating:</p>

                                <Rating initialRating={this.state.gameRating} onChange={this.gameRating} onHover={(rate) => {
                                    if(rate !== undefined){
                                        document.getElementById('label-onrate').innerHTML = rate+'/10'
                                    }
                                    else{
                                        document.getElementById('label-onrate').innerHTML = ''
                                    }}} stop={10} />
                                <p id="label-onrate"></p>
                            </FormGroup><br />

                            <Button style={{width: "100%", color: "white", backgroundColor: "black"}} disabled={isInvalid} type="submit">Add</Button>
                        </Modal.Body>
                    </form>
                </Modal>
            </div>
        );
    }
}

export default AddModal;