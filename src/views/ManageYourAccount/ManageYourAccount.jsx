import React, { Component } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import _ from "lodash";
import { Form, FormGroup, Input, Label, Button } from 'reactstrap';
// Authorization
import withAuthorization from "../../components/FirebaseAuthorization/withAuthorization";
// Firebase
import firebase,{ fire, auth } from "../../firebase/firebase";
import '../../assets/css/ManageYourAccount.css';
import logo from '../../logo.svg';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import axios from 'axios';


class ManageYourAccount extends Component{
  _isMounted = false;
  
  constructor(props) {
    super(props);
    this.state = {
        checkAllaccountArray: [],
        accountDetails: [],
        loading: true,
        currentpassword: "",
        newpassword: "",
        cfmnewpassword: ""
    };

    this.onAfterSaveCell = this.onAfterSaveCell.bind(this);
    this.handleChange = this.handleChange.bind(this);  
    this.cfmnewpassword = this.cfmnewpassword.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.changePW = this.changePW.bind(this);
  }

  componentWillMount() {
    this._isMounted = true;

    axios.get(process.env.REACT_APP_FIREBASE_DATABASE + "/RegisteredAccountDetails/" + 
    ".json?auth=" + process.env.REACT_APP_FIREBASE_DATABASE_SECRET)
    .then(response => {
      if(this._isMounted){
        var allAccount = [];

        _.map(response.data, i => {
          allAccount.push(i);
        })

        this.setState({ checkAllaccountArray: allAccount })
      }
    })

    fire.database().ref("RegisteredAccountDetails").child(auth.currentUser.uid).orderByChild("Name").once("value")
    .then(snapshot => {
      if(this._isMounted){
        var tempArray = [];

        tempArray.push(snapshot.val());

        this.setState({
            accountDetails: tempArray,
            loading: false
        });
      }
    })
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  loadingData() {
    if (this.state.loading) {
      return (
        <div>
          <h2>Loading...</h2>
        </div>
      )
    }
    else {
      return (
        <div>
          <h2>No Data Found</h2>
        </div>
      )
    }
  }

  onAfterSaveCell(row, cellName, cellValue) {
    var duplicate = [];
    var FoundData = "";
    var checkAllAccount = [];
    var regex = /^[a-z0-9" "]+$/i;  // For name: only letters, numbers and spacing
    var regexName = /^[" "]+$/i;    // For name: contain only spacing
    var timestamp = Math.floor(Date.now() / 1000);
    var actualDateTime = new Date(parseInt(timestamp, 0) * 1000).toString();

    if(cellName === "name")
    {
      this.state.checkAllaccountArray.forEach(childsnap => {
        if(childsnap.name === cellValue){
          checkAllAccount.push(true);
        }
        else{
          checkAllAccount.push(false);
        }
      })

      if(cellValue !== "" && regexName.test(cellValue) === false)
      {
        fire.database().ref("RegisteredAccountDetails").child(auth.currentUser.uid).orderByValue().once("value")
        .then(snapshot => {
          if(snapshot.val().name !== cellValue)
          {
            duplicate.push("No");
          }
          else
          {
            duplicate.push("Yes");
          }

          if(snapshot.val().name === row["nameId"])
          {
            FoundData = snapshot.val().name;
          }

          var duplicateName = duplicate.filter((value) => value === "Yes");
          var duplicateAccount = checkAllAccount.filter((value) => value === true);

          if(duplicateName[0] === "Yes" && FoundData === row["nameId"])
          {
            alert("Please ensure your Name is not duplicated!");
          }
          else
          {
            if(duplicateAccount[0] === true){
              this.setState({}) // Need this to trigger the code 'row["name"] = row["nameId"]'
              row["name"] = row["nameId"]
              alert("Sorry, The name is already taken!");
            }
            else{
              if(regex.test(cellValue) === true)
              {
                if(cellValue.length < 31)
                {
                  var Details = {
                      name: cellValue,
                      lastUpdate: actualDateTime,
                      lastUpdateTimestamp: parseInt(timestamp, 0)
                  }
            
                  fire.database().ref('RegisteredAccountDetails').child(auth.currentUser.uid).update(Details).then(()=>{window.location.reload()});
                }
                else
                {
                  this.setState({}) // Need this to trigger the code 'row["name"] = row["nameId"]'
                  row["name"] = row["nameId"]
                  alert("Sorry, Maximum of 30 characters only!");
                }
              }
              else
              {
                this.setState({}) // Need this to trigger the code 'row["name"] = row["nameId"]'
                row["name"] = row["nameId"]
                alert("Please enter alphanumeric only!");
              }
            }
          }
        })
      }
      else{
        this.setState({}) // Need this to trigger the code 'row["name"] = row["nameId"]'
        row["name"] = row["nameId"]
        alert("Please enter something to edit!");
      }
    }

    else if(cellName === "email")
    {
      duplicate = [];
      FoundData = "";
      regex = /^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/;  // For email: valid email include must contain/does not repeat "@" and "."

      this.state.checkAllaccountArray.forEach(childsnap => {
        if(childsnap.email === cellValue){
          checkAllAccount.push(true);
        }
        else{
          checkAllAccount.push(false);
        }
      })

      if(cellValue !== "")
      {
        fire.database().ref("RegisteredAccountDetails").child(auth.currentUser.uid).orderByValue().once("value")
        .then(snapshot => {
          if(snapshot.val().email !== cellValue)
          {
            duplicate.push("No");
          }
          else
          {
            duplicate.push("Yes");
          }

          if(snapshot.val().email === row["emailId"])
          {
            FoundData = snapshot.val().email;
          }

          var duplicateEmail = duplicate.filter((value) => value === "Yes");
          var duplicateAccount = checkAllAccount.filter((value) => value === true);

          if(duplicateEmail[0] === "Yes" && FoundData === row["emailId"])
          {
            alert("Please ensure your Email is not duplicated!");
          }
          else
          {
            if(duplicateAccount[0] === true){
              this.setState({}) // Need this to trigger the code 'row["email"] = row["emailId"]'
              row["email"] = row["emailId"]
              alert("Sorry, The Email is already taken!");
            }
            else{
              if(cellValue.length < 41)
              {
                if(regex.test(cellValue) === true)
                {
                  var Details = {
                      email: cellValue,
                      lastUpdate: actualDateTime,
                      lastUpdateTimestamp: parseInt(timestamp, 0)
                  };
              
                  fire.database().ref('RegisteredAccountDetails').child(auth.currentUser.uid).update(Details).then(()=>{window.location.reload()});
                }
                else
                {
                  this.setState({}) // Need this to trigger the code 'row["email"] = row["emailId"]'
                  row["email"] = row["emailId"]
                  alert("Please ensure your Email is valid!");
                }           
              }
              else
              {
                this.setState({}) // Need this to trigger the code 'row["email"] = row["emailId"]'
                row["email"] = row["emailId"]
                alert("Sorry, Maximum of 40 characters only!");
              }
            }
          }
        })
      }
      else{
        this.setState({}) // Need this to trigger the code 'row["email"] = row["emailId"]'
        row["email"] = row["emailId"]
        alert("Please enter something to edit!");
      }
    }

    else if(cellName === "contact")
    {
      duplicate = [];
      FoundData = "";
      regex = /^[0-9]+$/i;    // For contact: contain only numbers

      this.state.checkAllaccountArray.forEach(childsnap => {
        if(childsnap.contact === cellValue){
          checkAllAccount.push(true);
        }
        else{
          checkAllAccount.push(false);
        }
      })

      if(cellValue !== "")
      {
        fire.database().ref("RegisteredAccountDetails").child(auth.currentUser.uid).orderByValue().once("value")
        .then(snapshot => {
          if(snapshot.val().contact !== cellValue)
          {
            duplicate.push("No");
          }
          else
          {
            duplicate.push("Yes");
          }

          if(snapshot.val().contact === row["contactId"])
          {
            FoundData = snapshot.val().contact;
          }

          var duplicateContact = duplicate.filter((value) => value === "Yes");
          var duplicateAccount = checkAllAccount.filter((value) => value === true);

          if(duplicateContact[0] === "Yes" && FoundData === row["contactId"])
          {
            alert("Please ensure your Contact.No is not duplicated!");
          }
          else
          {
            if(duplicateAccount[0] === true){
              this.setState({}) // Need this to trigger the code 'row["contact"] = row["contactId"]'
              row["contact"] = row["contactId"]
              alert("Sorry, The Contact.No is already taken!");
            }
            else{
              if(cellValue.length > 7 && cellValue.length < 21)
              {
                if(regex.test(cellValue) === true)
                {
                  var Details = {
                      contact: cellValue,
                      lastUpdate: actualDateTime,
                      lastUpdateTimestamp: parseInt(timestamp, 0)
                  };
              
                  fire.database().ref('RegisteredAccountDetails').child(auth.currentUser.uid).update(Details).then(()=>{window.location.reload()});
                }
                else
                {
                  this.setState({}) // Need this to trigger the code 'row["contact"] = row["contactId"]'
                  row["contact"] = row["contactId"]
                  alert("Please ensure your Contact.No contains only numbers!");
                }           
              }
              else
              {
                this.setState({}) // Need this to trigger the code 'row["contact"] = row["contactId"]'
                row["contact"] = row["contactId"]
                alert("Sorry, Please enter 8-20 digits only!");
              }
            }
          }
        })
      }
      else{
        this.setState({}) // Need this to trigger the code 'row["contact"] = row["contactId"]'
        row["contact"] = row["contactId"]
        alert("Please enter something to edit!");
      }
    }
  }

  handleChange = text => {
    this.setState({ [text.target.name]: text.target.value });
  }

  cfmnewpassword(e){
    if(e.key === "Enter")
    {
        e.preventDefault();
        document.getElementById("submit").click();
    }
  }

  reAuthenticate = (currentPassword) => {
    var user = auth.currentUser
    var cred = firebase.auth.EmailAuthProvider.credential(user.email, currentPassword);
    return user.reauthenticateWithCredential(cred)
  }

  changePW(newpassword) {
    if(this.state.currentpassword === ""){
        alert("Please enter your Current Password!");
    }
    else{
        this.reAuthenticate(this.state.currentpassword).then(() => {
            if(this.state.currentpassword === newpassword){
                alert("Sorry, Current Password cannot be same as New Password");
            }
            else{
                var user = auth.currentUser;
                user.updatePassword(newpassword).then(() => {
                    alert('Password changed!!!');
                    document.location.reload();
                }).catch((error) => {
                    alert(error);
                });
            }
        })
        .catch(() => {
            alert("Sorry, Please check your Current Password again!");
        })
    }
  }

  handleReset() {
    this.setState({
      currentpassword: '',
      newpassword: '',
      cfmnewpassword: ''
    });
  }

  render() {
    const accountDetails = _.map(this.state.accountDetails, i => {
        return (
          {
            nameId: i.name,
            emailId: i.email,
            contactId: i.contact,
            name: i.name,
            email: i.email,
            contact: i.contact,
            created: i.createdOn,
            update: i.lastUpdate
          }
        )
    })
  
    const options = {
        noDataText: this.loadingData()
    };

    const cellEditProp = {
        mode: 'dbclick',
        blurToSave: true,
        afterSaveCell: this.onAfterSaveCell
    };

    return(
      <div className="App">
        <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 id="title">Manage Your Account</h1>
            <h6>You may only edit your Name, Email and Contact.No column by double-click the data!</h6><br />

            <Tabs>
              <TabList>
                <Tab>Edit Your Account Info</Tab>
                <Tab>Change Password</Tab>
              </TabList>

              <TabPanel><br />
                <BootstrapTable bordered={false} data={accountDetails} className="headertable" options={options} cellEdit={cellEditProp}>
                    <TableHeaderColumn dataField='nameId' hidden></TableHeaderColumn>
                    <TableHeaderColumn dataField='emailId' isKey={true} hidden></TableHeaderColumn>
                    <TableHeaderColumn dataField='contactId' hidden></TableHeaderColumn>
                    <TableHeaderColumn dataField='name' tdStyle={{ whiteSpace: 'normal' }}>Your Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='email' tdStyle={{ whiteSpace: 'normal' }}>Your Email</TableHeaderColumn>
                    <TableHeaderColumn dataField='contact' tdStyle={{ whiteSpace: 'normal' }}>Your Contact No</TableHeaderColumn>
                    <TableHeaderColumn dataField='created' editable={false} tdStyle={{ whiteSpace: 'normal' }}>Account Created On</TableHeaderColumn>
                    <TableHeaderColumn dataField='update' editable={false} tdStyle={{ whiteSpace: 'normal' }}>Last Update</TableHeaderColumn>
                </BootstrapTable><br /><br />
              </TabPanel>

              <TabPanel><br />
                <Form>
                    <FormGroup className="rowText">
                        <Label for="currentpassword">Current Password:</Label>

                        <Input
                            type="password"
                            name="currentpassword"
                            onChange={this.handleChange} />
                    </FormGroup>

                    <FormGroup className="rowText">
                        <Label for="newpassword">New Password:</Label>

                        <Input
                            type="password"
                            name="newpassword"
                            onChange={this.handleChange} />
                    </FormGroup>

                    <FormGroup className="rowText">
                        <Label for="cfmnewpassword">Confirm New Password:</Label>

                        <Input
                            type="password"
                            name="cfmnewpassword"
                            onChange={this.handleChange} 
                            onKeyUp={this.cfmnewpassword}/>
                    </FormGroup><br />

                    <Button id="submit" 
                        onClick={() => {
                            if (this.state.newpassword !== this.state.cfmnewpassword) {
                                alert('New Password and Confirm New Password do not match!');
                            }else if(this.state.newpassword === "" && this.state.cfmnewpassword === ""){
                                alert("Please enter your New Password and Confirm New Password!");
                            } 
                            else {
                                this.changePW(this.state.newpassword)
                            }
                        }}>
                        Confirm
                    </Button>

                    <Button id="reset" onClick={this.handleReset} type="reset">Reset</Button>
                </Form><br /><br />
              </TabPanel>
            </Tabs>
        </header>
      </div>
    )
  }
}

const authCondition = authUser => !!authUser;
export default withAuthorization(authCondition)(ManageYourAccount);