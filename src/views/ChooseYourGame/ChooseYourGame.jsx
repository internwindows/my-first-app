import React, { Component } from 'react';
import { Form, FormGroup, Label, Button, Input } from 'reactstrap';
import Switch from 'react-switch';
import _ from "lodash";
// Authorization
import withAuthorization from "../../components/FirebaseAuthorization/withAuthorization";
// Firebase
import { fire, auth } from "../../firebase/firebase";
import Multiselect from 'react-widgets/lib/Multiselect';
import '../../assets/css/ChooseYourGame.css';
import logo from '../../logo.svg';


class ChooseYourGame extends Component{
  _isMounted = false;
  
  constructor(props) {
    super(props);
    this.state = {
      charactername: "",
      GameList: [],
      selectedGameArray: [],
      solo: "",
      multiplayer: "",
      players: [],
      gender: "",
      design: false
    };

    this.AddToDatabase = this.AddToDatabase.bind(this);
  }

  componentWillMount() {
    this._isMounted = true;

    fire.database().ref("GameList").child(auth.currentUser.uid).orderByChild("Name").once("value")
    .then(snapshot => {
      if(this._isMounted){
        var tempArray = [];

        snapshot.forEach(snap => {
          tempArray.push(snap.val());
        });

        this.setState({
          GameList: tempArray
        });
      }
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  AddToDatabase(game){
    var design = "";
    var duplicate = [];             // Only for Character Name
    var regex = /^[a-z0-9" "]+$/i;  // For name: only letters, numbers and spacing
    var timestamp = Math.floor(Date.now() / 1000);
    var actualDateTime = new Date(parseInt(timestamp, 0) * 1000).toString();
    var randomKey = fire.database().ref().push().key;

    if(this.state.solo === "" && this.state.multiplayer !== ""){
      this.setState({
        players: this.state.multiplayer
      })
    }
    else if(this.state.solo !== "" && this.state.multiplayer === ""){
      this.setState({
        players: this.state.solo
      })
    }
    else{
      var player = this.state.solo + " and " + this.state.multiplayer;
      this.setState({
        players: player
      })
    }

    if(this.state.design === false){
      design = "2D";
    }
    else{
      design = "3D";
    }

    if(this.state.charactername.length > 30){
      alert("Sorry, Character Name only allows maximum of 30 characters!");
    }
    else{
      if(regex.test(this.state.charactername) === false){
        alert("Please enter your Charcter Name alphanumeric only!");
      }
      else{
        fire.database().ref("PlayerChoices").child(auth.currentUser.uid).orderByKey().once("value")
        .then(snapshot => {
          snapshot.forEach(snap => {
            if(snap.val().CharacterName === this.state.charactername){
              duplicate.push("Yes");
            }
            else{
              duplicate.push("No");
            }
          })

          var duplicateName = duplicate.filter((value) => value === "Yes");

          if(duplicateName[0] === "Yes")
          {
            alert("You have already created this Character Name! \nChange another Character Name?");
          }
          else{
            var MyGame = [];

            game.forEach(snap => {
              MyGame.push(snap.name);
            })

            setTimeout(() => {
              var Choices = {
                CharacterName: this.state.charactername,
                MyGame: MyGame.toString(),
                PreferGamePlay: this.state.players,
                PreferCharacterGender: this.state.gender,
                PreferinDesign: design,
                createdOn: actualDateTime,
                unixTimestamp: parseInt(timestamp, 0)
              }
      
      
              fire.database().ref("PlayerChoices").child(auth.currentUser.uid).child(randomKey).set(Choices)
              .then(() => {
                setTimeout(() => {
                  alert("Successfully Added");
                  window.location.reload();  
                }, 1000);
              })
            }, 500);
          }
        })
      }
    }
  }

  render() {
    const byPropKey = (propertyName, value) => () => ({
      [propertyName]: value
    });

    const selectedGame = _.map(this.state.GameList, data => {
      return (
        {
          id: data,
          name: data
        }
      )
    });

    return(
      <div className="App">
        <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 id="title">Choose Your Game</h1><br />

            <div>
              <Form>
                <FormGroup className="addgameinfobody">
                  <Label>What your Character Name:</Label>

                  <div className="addgameinfobodyright">
                    <Input
                      required={true}
                      type="text"
                      name="charactername"
                      onChange={event =>
                        this.setState(byPropKey("charactername", event.target.value))
                      } />
                  </div>
                </FormGroup>

                <FormGroup className="addgameinfobody">
                  <Label>Please select your game:</Label>

                  <div className="addgameinfobodyright">
                    <Multiselect
                      style={{minWidth: "260px"}}
                      data={selectedGame}
                      valueField='id'
                      textField='name'
                      onChange={selectedGameArray => this.setState({selectedGameArray})} />
                  </div>
                </FormGroup>

                <FormGroup className="addgameinfobody">
                  <Label>In Game, You prefer in:</Label>

                  <div className="addgameinfobodyright">
                    <input type="checkbox" name="Solo" onChange={events => {
                      if(events.target.checked){
                        this.setState({solo: "Solo"})
                      }else{
                        this.setState({solo: ""})
                      }}} />Solo
                    <input type="checkbox" name="MultiPlayer" style={{marginLeft: '20px'}} onChange={events => {
                      if(events.target.checked){
                        this.setState({multiplayer: "MultiPlayer"})
                      }else{
                        this.setState({multiplayer: ""})
                      }}} />MultiPlayer
                  </div>
                </FormGroup>

                <FormGroup className="addgameinfobody">
                  <Label>Which gender you prefer for your Game Character:</Label>

                  <div className="addgameinfobodyright">
                    <input type="radio" className="radiogender" name="gender" onClick={events => this.setState({gender: events.target.value})} value="Male" /> Male
                    <input type="radio" className="radiogender" name="gender" onClick={events => this.setState({gender: events.target.value})} style={{marginLeft: '20px'}} value="Female"/> Female
                  </div>
                </FormGroup>
                
                <FormGroup className="addgameinfobody">
                  <Label>What do you prefer in Design:</Label>

                  <div className="addgameinfobodyright">
                    <Switch
                      onChange={design => this.setState({design})} 
                      checked={this.state.design} 
                      id="normal-switch" />

                    <span>{this.state.design ?
                      <p style={{marginLeft: '10px'}}>3D</p>
                      :
                      <p style={{marginLeft: '10px'}}>2D</p>}
                    </span>
                  </div>
                </FormGroup><br />

                <Button id="submit" onClick={()=>{
                  if(this.state.charactername !== "" && this.state.selectedGameArray[0] !== undefined && (this.state.solo !== "" || this.state.multiplayer !== "")
                    && this.state.gender !== ""){
                    this.AddToDatabase(this.state.selectedGameArray)
                  }
                  else{
                    alert("Please select your choices for all fields!");
                  }
                }}>
                  Submit
                </Button>            
                <Button id="reset" onClick={() => {window.location.reload(false)}} type="reset">Reset</Button>
              </Form><br /><br />
            </div>
        </header>
      </div>
    )
  }
}

const authCondition = authUser => !!authUser;
export default withAuthorization(authCondition)(ChooseYourGame);