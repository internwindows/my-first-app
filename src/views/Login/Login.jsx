import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Form, Label, Input, Button } from 'reactstrap';
import logo from '../../logo.svg';
import '../../assets/css/Login.css';
import * as routes from '../../routes/routes';
import { auth } from '../../firebase/firebase';
import ForgotPasswordModal from "./ForgotPasswordModal";


const LoginPage = ({ history }) => (
    <div>
      <Login history={history} />
    </div>
);

class Login extends Component {
  constructor(props) {
    super(props);    
    this.state = {
        email: '',
        password: '',
        error: ''
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleReset = this.handleReset.bind(this);
  }

  emailorpassword(e){
    if(e.key === "Enter")
    {
        e.preventDefault();
        document.getElementById("submit").click();
    }
  }

  handleSubmit(event) {
    const { email, password } = this.state;
    const { history } = this.props;

    auth.signInWithEmailAndPassword(email, password)
    .then(() => {
        history.push(routes.Dashboard);
    }).catch(error => {
        this.setState({error: error});
        
        setTimeout(() => {
            this.setState({error: ''});
        }, 3000);
    })

    event.preventDefault();
  }

  handleReset() {
    this.setState({
      name: '',
      email: '',
      password: '',
      cfmpassword: ''
    });
  }

  render() {
    const byPropKey = (propertyName, value) => () => ({
      [propertyName]: value
    });
    
    const { email, password } = this.state;
    const { error } = this.state;

    return (
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />

            <h1 id="title">Login</h1><br />
            
            <Form>
                <div className="rowText">
                    <Label for="email">Email:</Label>

                    <Input
                      required={true}
                      type="email"
                      name="email"
                      value={email}
                      onChange={event =>
                        this.setState(byPropKey("email", event.target.value))
                      }
                      onKeyUp={this.emailorpassword} />
                </div>

                <div className="rowText">
                    <Label for="password">Password:</Label>
                    
                    <Input
                      value={password}
                      required={true}
                      type="password"
                      name="password"
                      onChange={event =>
                        this.setState(byPropKey("password", event.target.value))
                      } 
                      onKeyUp={this.emailorpassword} />
                </div>

                <div id="forget">
                  <ForgotPasswordModal /><br />
                </div>

                <Button id="submit" onClick={this.handleSubmit}>Login</Button>            
                <Button id="reset" onClick={this.handleReset} type="reset">Reset</Button>
            </Form><br />
            
            {error && <span id="error">{error.message}</span>}<br /><br />
          </header>
        </div>
    );
  }
}

export default withRouter(LoginPage);