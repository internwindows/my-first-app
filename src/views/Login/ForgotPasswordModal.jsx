import React, { Component } from 'react';
import { Button, Modal, FormGroup, FormControl } from 'react-bootstrap';
import { auth } from '../../firebase/firebase';
import '../../routes/routes';
import { Link } from '@material-ui/core';


function FieldGroup({ id, label, help, ...props }) {
    return (
        <FormGroup controlId={id}>
            <div>{label}</div>
            <FormControl {...props} />
            {help && <div>{help}</div>}
        </FormGroup>
    );
}

class ForgotPasswordModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            hidden: true,
            returnMessage: "",
            email: ''
        };

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleDisplay = this.handleDisplay.bind(this);
        this.handleHide = this.handleHide.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.forceUpdateHandler = this.forceUpdateHandler.bind(this);
    }

    forceUpdateHandler() {
        this.forceUpdate();
    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }

    handleHide() {
        this.setState({ hidden: true })
        this.forceUpdateHandler()
    }

    handleDisplay() {
        this.setState({ hidden: false })
        this.forceUpdateHandler()
    }

    onSubmit(formEvent) {
        formEvent.preventDefault();
        var email = this.state.email;

        auth.sendPasswordResetEmail(email).then(() => {
            this.setState({
                returnMessage: "Please check your email address for reset instructions",
            });

            setTimeout(() => {
                this.setState({returnMessage: ''});
              }, 3000);
        }).catch((error) => {
            this.setState({
                returnMessage: error.message
            });

            setTimeout(() => {
                this.setState({returnMessage: ''});
              }, 3000);
        });
    }

    render() {
        const { email } = this.state;
        const isInvalid = email === '';

        return (
            <div>
                <Link href="#" onClick={this.handleShow} style={{color: "lightblue"}}>
                     Forgot Password?
                </Link>

                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton >
                        <Modal.Title>Reset Password</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <form onSubmit={this.onSubmit}>
                            <FieldGroup
                                id="formControlsEmail"
                                type="email"
                                label="Email address"
                                onChange={(event) => { this.setState({ email: event.target.value })}}
                            /><br />

                            <Button style={{width: "100%", color: "white", backgroundColor: "black"}} disabled={isInvalid} type="submit" >Reset Password</Button>
                        </form><br />

                        <p style={{ textAlign: "center" }}>{this.state.returnMessage}</p>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}

export default ForgotPasswordModal;