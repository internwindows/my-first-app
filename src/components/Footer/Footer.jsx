import React, { Component } from "react";
import { Col } from "react-bootstrap";


class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <div>
          <Col md={12}>
            <p className="copyright" style={{textAlign: "center"}}>
              &copy; {new Date().getFullYear()}{" "}
              <a href="https://reactstrap.github.io/" target="new">My First App</a>
            </p>
          </Col>
        </div>
      </footer>
    );
  }
}

export default Footer;
