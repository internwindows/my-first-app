import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { HeaderLinks } from '../Header/Header';
import "../../routes/routes";
import AuthUserContext from "../CheckFirebaseAuth/AuthUserContext";
import { Link } from '@material-ui/core';


const Navigation = () => (
  <AuthUserContext.Consumer>
    {authUser => (authUser ? <NavigationAuth /> : <NavigationNonAuth />)}
  </AuthUserContext.Consumer>
);

const NavigationNonAuth = () => {
  return (
    <div className="nav">
      <li>
        <NavLink to="/login" className="nav-link" style={{minWidth: "230px"}}>
          <i className="pe-7s-next-2" />
          <p>Log in</p>
        </NavLink>
      </li>
      <li>
        <NavLink to="/signup" className="nav-link" style={{minWidth: "230px"}}>
          <i className="pe-7s-add-user" />
          <p>Sign up</p>
        </NavLink>
      </li>
    </div>
  );
};

const NavigationAuth = () => {
  return (
    <div className="nav">
      <li>
        <NavLink to="/dashboard" className="nav-link" style={{minWidth: "230px"}}>
          <i className="pe-7s-global" />
          <p>Dashboard</p>
        </NavLink>
      </li>
      <li>
        <NavLink to="/addyourgame" className="nav-link" style={{minWidth: "230px"}}>
          <i className="pe-7s-plus" />
          <p>Add your Game</p>
        </NavLink>
      </li>
      <li>
        <NavLink to="/chooseyourgame" className="nav-link" style={{minWidth: "230px"}}>
          <i className="pe-7s-joy" />
          <p>Choose your Game</p>
        </NavLink>
      </li>
      <li>
        <NavLink to="/editthegamechoices" className="nav-link" style={{minWidth: "230px"}}>
          <i className="pe-7s-note" />
          <p>Edit the Game Choices</p>
        </NavLink>
      </li>
      <li>
        <NavLink to="/manageyouraccount" className="nav-link" style={{minWidth: "230px"}}>
          <i className="pe-7s-id" />
          <p>Manage Your Account</p>
        </NavLink>
      </li>
    </div>
  );
};

class Sidebar extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      width: window.innerWidth,
      authUser: null
    };
  }

  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
  }

  updateDimensions() {
    this.setState({ width: window.innerWidth });
  }

  componentDidMount() {
    this._isMounted = true;
    if(this._isMounted){
      this.updateDimensions();
      window.addEventListener("resize", this.updateDimensions.bind(this));
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    return (
      <div id="sidebar" className="sidebar" data-color="black">
        <div className="sidebar-wrapper">
          <ul className="nav">
            <li>
              <Link href="https://themes-pixeden.com/font-demos/7-stroke/" target="new" 
              className="nav-link" style={{minWidth: "230px", backgroundColor: "red", textAlign: "center"}}>
                <p>My First App</p>
              </Link>
            </li>

            {this.state.width <= 991 ? <HeaderLinks /> : null}
            <Navigation />
          </ul>
        </div>
      </div>
    );
  }
}

export default Sidebar;