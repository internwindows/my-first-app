import React, { Component } from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { fire, auth } from '../../firebase/firebase';
import '../../routes/routes';
import AuthUserContext from '../CheckFirebaseAuth/AuthUserContext';


export const HeaderLinks = () => (
  <AuthUserContext.Consumer>
    {authUser => (authUser ? <HeaderAuth /> : <HeaderNonAuth />)}
  </AuthUserContext.Consumer>
);

const HeaderAuth = () => {
  return (
    <div>
      <Nav style={{float: "right"}}>
        <LinkContainer to="/login" onClick={() => auth.signOut()}>
          <NavItem>
            <h4>
                Log Out
            </h4>
          </NavItem>
        </LinkContainer>
      </Nav>
    </div>
  );
};

const HeaderNonAuth = () => {
  return (
    <div>
      <Nav />
    </div>
  );
};

class Header extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      loading: true
    };

    this.mobileSidebarToggle = this.mobileSidebarToggle.bind(this);
  }

  mobileSidebarToggle(e) {
    e.preventDefault();
    document.documentElement.classList.toggle("nav-open");
    var node = document.createElement("div");
    node.id = "bodyClick";
    node.onclick = function () {
      this.parentElement.removeChild(this);
      document.documentElement.classList.toggle("nav-open");
    };
    document.body.appendChild(node);
  }

  componentWillMount() {
    this._isMounted = true;

    fire.auth().onAuthStateChanged((user) => {
      if (user) {
        // User logged in already or has just logged in.
        fire.database().ref("RegisteredAccountDetails").child(auth.currentUser.uid).once("value")
        .then(async (snapshot) => {
          if(this._isMounted){
            try{
              await this.setState({
                email: snapshot.val().email,
                loading: false
              })
            }
            catch(error){
              console.log(error.message)
            }
          }
        })
      } else {
        // User not logged in or has just logged out.
        if(this._isMounted){
          this.setState({ 
            email: "",
            loading: true
          })
        }
      }
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  namedisplay() {
    if (this.state.loading) {
      return (
        <div style={{float: "left", marginTop: "55px", fontWeight: "bold", fontSize: "20px"}}>
          {/* Empty String, Applied for both NonAuth and Auth Account */}
        </div>
      )
    } else {
      return (
        <div style={{float: "left", marginTop: "55px", fontWeight: "bold", fontSize: "20px"}}>
          Welcome, {this.state.email}
        </div>
      )
    }
  }

  render() {
    return(
      <Navbar fluid>
        <Navbar.Header>
          <Navbar.Toggle onClick={this.mobileSidebarToggle} />
        </Navbar.Header>

        <Navbar.Collapse>
          {this.namedisplay()}
          <HeaderLinks />
        </Navbar.Collapse>
      </Navbar>
    )
  }
}

export default Header;