import React from 'react';
import { withRouter } from 'react-router-dom';
import AuthUserContext from '../CheckFirebaseAuth/AuthUserContext';
import { auth } from '../../firebase/firebase';
import * as routes from '../../routes/routes';


const withAuthorization = authCondition => Component => {
  class WithAuthorization extends React.Component {
    _isMounted = false;

    componentDidMount() {
      this._isMounted = true;

      auth.onAuthStateChanged(authUser => {
        if(this._isMounted){
          if (!authCondition(authUser)) {
            this.props.history.push(routes.Login);
          }
        }
      });
    }

    componentWillUnmount() {
      this._isMounted = false;
    }
    
    render() {
      return (
        <AuthUserContext.Consumer>
          {authUser => (authUser ? <Component /> : null)}
        </AuthUserContext.Consumer>
      );
    }
  }
  return withRouter(WithAuthorization);
};

export default withAuthorization;