import React from 'react';
import AuthUserContext from './AuthUserContext';
import { auth } from '../../firebase/firebase';


const checkAuth = (Component) => {
    class CheckAuth extends React.Component {
        _isMounted = false;

        constructor(props) {
            super(props)
            this.state = {
                authUser: auth.currentUser
            }
        }

        componentDidMount() {
            this._isMounted = true;

            auth.onAuthStateChanged(authUser => {
                if(this._isMounted){
                    authUser
                    ? this.setState(() => ({ authUser }))
                    : this.setState(() => ({ authUser: null }))
                }
            })
        }

        componentWillUnmount() {
            this._isMounted = false;
        }
        
        render() {
            const { authUser } = this.state;

            return (
                <AuthUserContext.Provider value={ authUser }>
                    <Component />
                </AuthUserContext.Provider>
            )
        }
    }

    return CheckAuth;
}

export default checkAuth;