import * as firebase from 'firebase';

const prodconfig = {
    apiKey: process.env.REACT_APP_FIREBASE_KEY,
    authDomain: process.env.REACT_APP_FIREBASE_DOMAIN,
    databaseURL: process.env.REACT_APP_FIREBASE_DATABASE,
    projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
    storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_FIREBASE_SENDER_ID
};

const devconfig = {
    apiKey: process.env.REACT_APP_FIREBASE_KEY,
    authDomain: process.env.REACT_APP_FIREBASE_DOMAIN,
    databaseURL: process.env.REACT_APP_FIREBASE_DATABASE,
    projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
    storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_FIREBASE_SENDER_ID
};


export const config = process.env.NODE_ENV === 'production'
    ? prodconfig
    : devconfig

export const fire = firebase.initializeApp(config);

export const auth = firebase.auth()
export const db = firebase.database()
export const dbRef = firebase.database().ref()

// If need to change the API key for the firebase, Please go to the .env file to change it

// auth.createUserWithEmailAndPassword(email, password) -> Signing Up via email and password

// new firebase.auth.FacebookAuthProvider()             -> Signing in via facebook

// auth.signInWithEmailAndPassword(email, password)     -> Log in via email and password

// auth.signOut()                                       -> Signing out

export default firebase