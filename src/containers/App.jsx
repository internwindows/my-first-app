import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
// Navigation
import Header from "../components/Header/Header";
import Sidebar from "../components/Sidebar/Sidebar";
import Footer from "../components/Footer/Footer";
// Non Auth Users
import Login from "../views/Login/Login";
import Signup from "../views/Signup/Signup";
// Auth Users
import Dashboard from "../views/Dashboard/Dashboard";
import AddYourGame from "../views/AddYourGame/AddYourGame";
import ChooseYourGame from "../views/ChooseYourGame/ChooseYourGame";
import EditTheGameChoices from "../views/EditTheGameChoices/EditTheGameChoices";
import ManageYourAccount from "../views/ManageYourAccount/ManageYourAccount";
import PageNotFound from "../views/Errors/Errors";
// routes
import "../routes/routes";
//HttpsRedirect
import HttpsRedirect from "react-https-redirect";
//Authentication
import withAuth from "../components/CheckFirebaseAuth/checkAuth";


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
        authUser: null
    };
  }

  render() {
    return (
      <HttpsRedirect>
        <div className="wrapper">
          <Sidebar {...this.props} />
          
          <div id="main-panel" className="main-panel">
            <Header {...this.props} />

            <div>
              <Switch>
                <Route exact path="/login" component={Login} />
                <Route exact path="/signup" component={Signup} />
                <Route exact path="/dashboard" component={Dashboard} />
                <Route exact path="/addyourgame" component={AddYourGame} />
                <Route exact path="/chooseyourgame" component={ChooseYourGame} />
                <Route exact path="/editthegamechoices" component={EditTheGameChoices} />
                <Route exact path="/manageyouraccount" component={ManageYourAccount} />
                <Route exact path="/" component={Dashboard} />
                <Route component={PageNotFound} />
              </Switch>
            </div>
            <Footer />
          </div>
        </div>
      </HttpsRedirect>
    );
  }
}

export default withAuth(App);